Meteor.startup(function () {
    if (Clients.find().count() === 0 ) {
        
        var collectionString = Assets.getText('Clients.json');
        var data = JSON.parse(collectionString);
        
        data.forEach(function (item, index, array) {
            Clients.insert(item);
        });
    };
});