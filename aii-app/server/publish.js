Meteor.publish('GetClients', function(options, searchString) {
    if (!searchString || searchString == null) {
        searchString = '';
    }
    
    let regexExpression = { '$regex': '.*' + searchString || '' + '.*', '$options': 'i' };    
    let selector = {
        $or:
        [
            { identification:   regexExpression },
            { name:             regexExpression },
            { lastName:         regexExpression },
            { email:            regexExpression }
        ]
    }
    
    Counts.publish(this, 'numberOfClients', Clients.find(selector), { noReady: true });
    return Clients.find(selector, options);
})