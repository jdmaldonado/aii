(function() {
  'use strict';

  angular
    .module('aiiApp')
    .config(routeConfig);

  function routeConfig( $urlRouterProvider, $stateProvider, $locationProvider) {

    $locationProvider.html5Mode(true);
    
    $stateProvider

    // Clients
        // List
        .state('clients',{
            url: '/clients',
            templateUrl : 'client/clients/clients-list/clients-list.ng.html',
            controller : 'ClientsListController as clientsList'
        })
        
        .state('addClient',{
            url: '/clients/create',
            templateUrl : 'client/clients/client-form/client-form.ng.html',
            controller : 'ClientFormController as clientForm'
        })
        
        // Detail
        // .state('clientDetail',{
        //     url: '/client/:clientId',
        //     templateUrl : 'client/app/clients/client-detail/client-detail.html',
        //     controller : 'ClientDetailController as clientDetail'
        // })

    $urlRouterProvider.otherwise("/clients");
  }

})();