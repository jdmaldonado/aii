(function() {
  'use strict';

  angular
    .module('aiiApp', ['angular-meteor', 'ui.router', 'angularUtils.directives.dirPagination'])

})();