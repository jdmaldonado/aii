(function() {
   'use strict';
   angular
       .module('aiiApp')
       .controller('ClientFormController', ClientFormController);

   function ClientFormController($meteor, $scope, $reactive) {
       var vm = this
       $reactive(this).attach($scope);
       
       vm.helpers({
            name: () => {
                return  "Juan David Maldonado";
            }
        });
       
   }

})();