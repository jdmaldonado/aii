(function() {
    'use strict';
    angular
        .module('aiiApp')
        .controller('ClientsListController', ClientsListController);
    
    function ClientsListController($meteor, $scope, $reactive) {
        var vm =  this;
        $reactive(this).attach($scope);

        vm.sort     = {};
        vm.page     = 1;
        vm.perPage  = 10;
        vm.searchText  = '';
        vm.fields   = {
            'identification' : 1,
            'name' : 1,
            'lastName': 1,
            'phone' : 1,
            'email' : 1
        };
        
        vm.subscribe('GetClients', () => {
            return [
                {
                    limit   : parseInt(vm.perPage),
                    skip    : parseInt((vm.getReactively('page') - 1) * vm.perPage ),
                    sort    : vm.getReactively('sort')
                },
                vm.getReactively('searchText')
            ]
        });

        vm.helpers({
            clients: () => {
                return  Clients.find({}, {
                    sort: vm.getReactively('sort'),
                    fields: vm.getReactively('fields')
                });
            },
            clientsCount: () => {
                return Counts.get('numberOfClients');
            }
        });

        // Order By Column selected
        vm.changeSort = (column) => {
            var newOrder = {};
            newOrder[column] = vm.sort[column] ? vm.sort[column] * -1 : 1 ;
            
            vm.sort = newOrder;
        };
        
        vm.changePage = (newPage) => {
            vm.page = newPage;
        }
        
        MakeTableResizable();
        
        // How the table is has header fixed, and body scrollable, it need this script
        // for resize the columns and make it responsive
        function MakeTableResizable() {
            var $table = $('table.scroll');
            var $bodyCells = $table.find('tbody tr:first').children();

            // Adjust the width of thead cells when window resizes
            $(window).resize(function() {
                // Get the tbody columns width array
                var colWidth = $bodyCells.map(function() {
                    return $(this).width();
                }).get();
                
                // Set the width of thead columns
                $table.find('thead tr').children().each(function(i, v) {
                    $(v).width(colWidth[i]);
                });    
            }).resize();
        };
    };
})()